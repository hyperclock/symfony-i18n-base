
/* this checks the localStorage if it already has the variable saved */
function checkAcceptCookies () {
  if (localStorage.aceptaCookies == 'true') {
    cajacookies.style.display = 'none';
  }
}

/* here we store the variable that has been
accepted the use of cookies so we will not show
the message again */
function acceptCookies () {
  localStorage.aceptaCookies = 'true';
  cajacookies.style.display = 'none';
}

/* this runs when the web is loaded */
$ (document) .ready (function () {
  checkAcceptCookies ();
});
